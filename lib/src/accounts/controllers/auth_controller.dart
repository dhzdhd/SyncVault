import 'dart:convert';

import 'package:fpdart/fpdart.dart';
import 'package:hive/hive.dart';
import 'package:syncvault/src/accounts/models/auth_provider_model.dart';
import 'package:syncvault/src/accounts/models/drive_info_model.dart';
import 'package:syncvault/src/accounts/services/auth/dropbox.dart';
import 'package:syncvault/src/accounts/services/auth/gdrive.dart';
import 'package:syncvault/src/accounts/services/auth/onedrive.dart';
import 'package:syncvault/errors.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'auth_controller.g.dart';

enum AuthProviderType {
  oneDrive,
  dropBox,
  googleDrive,
}

@riverpod
class Auth extends _$Auth {
  @override
  List<AuthProviderModel> build() {
    return init();
  }

  static List<AuthProviderModel> init() {
    final List<dynamic> raw =
        jsonDecode(Hive.box('vault').get('accounts', defaultValue: '[]'));

    try {
      return raw.map((e) => AuthProviderModel.fromJson(e)).toList();
    } catch (e) {
      return [];
    }
  }

  TaskEither<AppError, ()> signIn(AuthProviderType provider) {
    return switch (provider) {
      AuthProviderType.oneDrive => OneDriveAuth().signIn(),
      AuthProviderType.dropBox => DropBoxAuth().signIn(),
      AuthProviderType.googleDrive => GoogleDriveAuth().signIn(),
    }
        .chainEither(
      (model) {
        if (state.any((element) =>
            (element.provider == model.provider) &&
            (element.email == model.email))) {
          return Left(GeneralError(
            message: 'The provider already exists',
            stackTrace: '',
          ));
        }

        state = [...state, model];
        Hive.box('vault').put(
          'accounts',
          jsonEncode(state.map((e) => e.toJson()).toList()),
        );

        return const Right(());
      },
    );
  }

  TaskEither<AppError, AuthProviderModel> refresh(AuthProviderModel model) {
    return switch (model.provider) {
      AuthProviderType.oneDrive => OneDriveAuth().refresh(model),
      AuthProviderType.dropBox => DropBoxAuth().refresh(model),
      AuthProviderType.googleDrive => GoogleDriveAuth().refresh(model),
    }
        .map(
      (r) {
        state = [...state.where((e) => e != model), r];
        return r;
      },
    );
  }

  void signOut(AuthProviderModel model) async {
    state = state.where((element) => element != model).toList();
    Hive.box('vault').put(
      'accounts',
      jsonEncode(state.map((e) => e.toJson()).toList()),
    );
  }

  Future<Either<AppError, DriveInfoModel>> getDriveInfo(
    AuthProviderModel model,
  ) async {
    // Improve with a TaskEither
    return await (await refresh(model).run()).bindFuture((e) async {
      return switch (e.provider) {
        AuthProviderType.oneDrive =>
          await OneDriveAuth().getDriveInfo(e.accessToken).run(),
        AuthProviderType.dropBox =>
          await DropBoxAuth().getDriveInfo(e.accessToken).run(),
        AuthProviderType.googleDrive =>
          await GoogleDriveAuth().getDriveInfo(e.accessToken).run(),
      };
    }).run();
  }
}
